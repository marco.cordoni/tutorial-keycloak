package service;

import dto.token.Token;
import dto.UserDto;
import enums.TokenEnum;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Form;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import restclient.KeycloakClient;

@ApplicationScoped
public class KeycloakTokenService {

    @Inject
    @RestClient
    private KeycloakClient keycloakClient;

    @ConfigProperty(name = "quarkus.oidc.client-id")
    private String clientId;

    @ConfigProperty(name = "quarkus.oidc.credentials.secret")
    private String secret;

    private Form createBaseForm() {
        return new Form()
            .param(TokenEnum.CLIENT_ID.getValue(), clientId)
            .param(TokenEnum.CLIENT_SECRET.getValue(), secret);
    }

    public Token getToken(UserDto user) {
        Form params = createBaseForm()
            .param(TokenEnum.GRANT_TYPE.getValue(), TokenEnum.PASSWORD.getValue())
            .param(TokenEnum.USERNAME.getValue(), user.getUsername())
            .param(TokenEnum.PASSWORD.getValue(), user.getPassword());

        return keycloakClient.getToken(params);
    }

    public Token refreshToken(String refreshToken) {
        Form params = createBaseForm()
            .param(TokenEnum.GRANT_TYPE.getValue(), TokenEnum.REFRESH_TOKEN.getValue())
            .param(TokenEnum.REFRESH_TOKEN.getValue(), refreshToken);

        return keycloakClient.refreshToken(params);
    }

    public String introspectToken(String token) {
        Form params = createBaseForm()
            .param(TokenEnum.TOKEN.getValue(), token);
        return keycloakClient.introspectToken(params);
    }

    public String logout(String refreshToken) {
        Form params = createBaseForm()
            .param(TokenEnum.REFRESH_TOKEN.getValue(), refreshToken);
        return keycloakClient.logout(params);
    }
}
