package service;

import dto.BookDto;
import entity.BookEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import mapper.book.BookMapper;
import repository.BookRepository;

import java.util.List;

@ApplicationScoped
public class BookService {

    @Inject
    private BookRepository bookRepository;

    @Inject
    private AuthorService authorService;

    @Inject
    private BookMapper bookMapper;

    public List<BookDto> findAll() {
        List<BookEntity> listEntity = bookRepository.listAll();
        return bookMapper.mapEntityListToDtoList(listEntity);
    }

    public BookDto saveBook(BookDto bookDto) {
        if(bookDto.getId() != null) {
            throw new IllegalArgumentException("The book to save can not have an ID");
        }

        BookEntity bookEntity = bookMapper.map(bookDto);
        bookEntity = bookRepository.persist(bookEntity);
        return bookMapper.map(bookEntity);
    }
}
