package service;

import dto.AuthorDto;
import entity.AuthorEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import mapper.author.AuthorMapper;
import mapper.book.BookMapper;
import repository.AuthorRepository;

import java.util.List;

@ApplicationScoped
public class AuthorService {

    @Inject
    private AuthorRepository authorRepository;

    @Inject
    private AuthorMapper authorMapper;

    @Inject
    private BookMapper bookMapper;

    public List<AuthorDto> findAll() {
        List<AuthorEntity> authorEntityList = authorRepository.listAll();
        return authorMapper.mapEntityListToDtoListWithBooks(authorEntityList);
    }
}
