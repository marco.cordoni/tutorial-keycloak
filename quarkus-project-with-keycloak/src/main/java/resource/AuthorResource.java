package resource;

import dto.AuthorDto;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jboss.logging.Logger;
import service.AuthorService;

import java.util.List;

@Path("/api/authors")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class AuthorResource {

    @Inject
    private Logger logger;

    @Inject
    private AuthorService authorService;

    @GET
    public Response findAll() {
        logger.info("Find all authors");
        List<AuthorDto> authorDtoList = authorService.findAll();
        return Response.status(Response.Status.OK).entity(authorDtoList).build();
    }
}
