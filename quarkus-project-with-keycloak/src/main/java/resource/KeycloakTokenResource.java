package resource;

import dto.token.Token;
import dto.UserDto;
import jakarta.annotation.security.PermitAll;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.jboss.logging.Logger;
import service.KeycloakTokenService;

@Path("/keycloak/token")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class KeycloakTokenResource {

    @Inject
    private Logger logger;

    @Inject
    private KeycloakTokenService keycloakTokenService;

    @POST
    @PermitAll
    public Response getToken(@RequestBody UserDto user) {
        logger.infof("Get token for user [%s]", user.getUsername());
        Token response = keycloakTokenService.getToken(user);
        return Response.status(Response.Status.OK).entity(response).build();
    }

    @POST
    @Path("/refresh")
    @Consumes(MediaType.TEXT_PLAIN)
    @PermitAll
    public Response refreshToken(@RequestBody String refreshToken) {
        logger.infof("Refresh token");
        Token response = keycloakTokenService.refreshToken(refreshToken);
        return Response.status(Response.Status.OK).entity(response).build();
    }

    @POST
    @Path("/introspect")
    @Consumes(MediaType.TEXT_PLAIN)
    @PermitAll
    public Response introspectToken(@RequestBody String token) {
        logger.infof("Introspect token");
        String response = keycloakTokenService.introspectToken(token);
        return Response.status(Response.Status.OK).entity(response).build();
    }

    @POST
    @Path("/logout")
    @Consumes(MediaType.TEXT_PLAIN)
    @PermitAll
    public Response logout(@RequestBody String refreshToken) {
        logger.infof("Introspect token");
        String response = keycloakTokenService.logout(refreshToken);
        return Response.status(Response.Status.OK).entity(response).build();
    }
}
