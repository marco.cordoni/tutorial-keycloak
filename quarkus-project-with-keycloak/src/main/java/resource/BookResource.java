package resource;

import dto.BookDto;
import enums.RolesEnum;
import io.quarkus.security.Authenticated;
import jakarta.annotation.security.PermitAll;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.jboss.logging.Logger;
import service.BookService;

import java.util.List;

@Path("/api/books")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@Authenticated // with this annotation all the endpoints in this class require authentication
public class BookResource {

    @Inject
    private Logger logger;

    @Inject
    private BookService bookService;

    @GET
    @Path("/findAllWithoutAuthentication")
    @PermitAll // this annotation allow to use this api without authentication
    public Response findAllWithoutAuthentication() {
        logger.info("Find all books");
        List<BookDto> bookDtoList = bookService.findAll();
        return Response.status(Response.Status.OK).entity(bookDtoList).build();
    }

    @GET
    @Path("/findAllWithAuthentication")
    @SecurityRequirement(name = "Keycloak")
    public Response findAllWithAuthentication() {
        logger.info("Find all books");
        List<BookDto> bookDtoList = bookService.findAll();
        return Response.status(Response.Status.OK).entity(bookDtoList).build();
    }

    @GET
    @Path("/findAllWithAuthenticationAndRoleManager")
    @RolesAllowed(RolesEnum.MANAGER)
    public Response findAllWithAuthenticationAndRoleManager() {
        logger.info("Find all books");
        List<BookDto> bookDtoList = bookService.findAll();
        return Response.status(Response.Status.OK).entity(bookDtoList).build();
    }

    @POST
    @RolesAllowed(RolesEnum.ADMIN)
    public Response saveBook(BookDto bookDtoToSave) {
        logger.info("Save book");
        BookDto bookDtoSaved = bookService.saveBook(bookDtoToSave);
        return Response.status(Response.Status.CREATED).entity(bookDtoSaved).build();
    }
}
