package enums;

import lombok.Getter;

@Getter
public enum TokenEnum {

    CLIENT_ID("client_id"),
    CLIENT_SECRET("client_secret"),
    GRANT_TYPE("grant_type"),
    USERNAME("username"),
    PASSWORD("password"),
    REFRESH_TOKEN("refresh_token"),
    TOKEN("token");

    private final String value;

    TokenEnum(String value) {
        this.value = value;
    }

}
