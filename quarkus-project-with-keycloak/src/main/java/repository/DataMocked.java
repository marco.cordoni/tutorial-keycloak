package repository;

import entity.AuthorEntity;
import entity.BookEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DataMocked {

    private static List<BookEntity> bookEntityList;
    private static List<AuthorEntity> authorEntityList;

    private static void setUp() {
        bookEntityList = new ArrayList<>();
        BookEntity bookEntity1 = new BookEntity(
                "Ready Player One",
                null,
                LocalDate.of(2011, 9, 16)
        );
        bookEntityList.add(bookEntity1);

        authorEntityList = new ArrayList<>();
        AuthorEntity authorEntity1 = new AuthorEntity(
                "Ernest Cline",
                null,
                LocalDate.of(1972, 3, 29),
                bookEntityList
        );
        authorEntityList.add(authorEntity1);

        bookEntity1.setAuthor(authorEntity1);
    }

    public static List<BookEntity> listAllMockedBooks() {
        setUp();
        return bookEntityList;
    }

    public static List<AuthorEntity> listAllMockedAuthors() {
        setUp();
        return authorEntityList;
    }
}
