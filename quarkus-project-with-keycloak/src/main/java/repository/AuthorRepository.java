package repository;

import entity.AuthorEntity;
import entity.BookEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class AuthorRepository {

    public List<AuthorEntity> listAll() {
        List<BookEntity> bookEntityList = new ArrayList<>();
        BookEntity bookEntity1 = new BookEntity(
                "Ready Player One",
                null,
                LocalDate.of(2011, 9, 16)
        );
        bookEntityList.add(bookEntity1);

        List<AuthorEntity> authorEntityList = new ArrayList<>();
        AuthorEntity authorEntity1 = new AuthorEntity(
                "Ernest Cline",
                null,
                LocalDate.of(1972, 3, 29),
                bookEntityList
        );
        authorEntityList.add(authorEntity1);

        bookEntity1.setAuthor(authorEntity1);

        return authorEntityList;
    }
}
