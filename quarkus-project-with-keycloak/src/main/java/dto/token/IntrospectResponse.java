package dto.token;

import lombok.Getter;

@Getter
public class IntrospectResponse {
    private String active;
}
