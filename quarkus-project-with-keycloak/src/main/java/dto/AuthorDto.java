package dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@RegisterForReflection
public class AuthorDto extends BaseDto {
    private Integer id;
    private String name;
    private String alias;
    private List<BookDto> books = new ArrayList<>();

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
}
