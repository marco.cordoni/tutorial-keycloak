package dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import jakarta.json.bind.annotation.JsonbDateFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@RegisterForReflection
public class BookDto extends BaseDto{
    private Integer id;
    private String name;
    private AuthorDto author;

    @JsonbDateFormat("yyyy-MM-dd")
    private LocalDate publicationDate;
}
