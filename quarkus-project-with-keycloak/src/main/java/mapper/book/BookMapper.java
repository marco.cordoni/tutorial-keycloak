package mapper.book;

import dto.AuthorDto;
import dto.BookDto;
import entity.AuthorEntity;
import entity.BookEntity;
import mapper.UtilsMapper;
import mapper.author.AuthorMapper;
import org.mapstruct.*;

import java.util.List;

@Mapper(
        componentModel = "jakarta",
        uses = {UtilsMapper.class, AuthorMapper.class},
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public abstract class BookMapper {

    // After the map method, this function is automatically applied on the target object
    // this works both with the single object BookDto and with the list of BookDto
    @AfterMapping
    protected void convertBookNameToUpperCase(@MappingTarget BookDto bookDto) {
        bookDto.setName(bookDto.getName().toUpperCase());
    }

    // ---------------------- MAPPING WITH CUSTOM MAPPING FOR 1 FIELD ----------------------
    @Named("mapBookEntityToDto")
    @Mapping(source = "author", target = "author", qualifiedByName = "mapAuthorEntityToDtoWithoutBooks")
    public abstract BookDto map(BookEntity book);

    @Named("mapBookDtoToEntity")
    @Mapping(source = "author", target = "author", qualifiedByName = "mapAuthorDtoToEntity")
    public abstract BookEntity map(BookDto book);

    @Named("mapAuthorEntityToDto")
    @Mapping(source = "books", target = "books", qualifiedByName = "mapEntityListToDtoWithoutAuthor")
    public abstract AuthorDto map(AuthorEntity value);

    // ---------------------- MAPPING IGNORING USELESS FIELDS ----------------------

    @Named("mapBookEntityWithoutId")
    @Mapping(source = "id", target = "id", ignore = true)
    public abstract BookEntity mapBookEntityWithoutId(BookEntity book);


    // ---------------------- MAPPING LIST USING A SPECIF MAPPER FOR EACH ELEMENT ----------------------
    @Named("mapEntityListToDtoList")
    @IterableMapping(qualifiedByName = "mapBookEntityToDto")
    public abstract List<BookDto> mapEntityListToDtoList(List<BookEntity> list);

    @Named("mapEntityListToDtoWithoutAuthor")
    @IterableMapping(qualifiedByName = "mapBookEntityWithoutAuthor")
    public abstract List<BookDto> mapEntityListToDtoWithoutAuthor(List<BookEntity> list);

    @IterableMapping(qualifiedByName = "mapBookDtoToEntity")
    public abstract  List<BookEntity> mapDtoListToEntityList(List<BookDto> list);


    // ---------------------- TOTALLY CUSTOM MAPPER ----------------------
    @Named("mapBookEntityToDtoOnlyName") 
    public BookDto mapBookEntityToDtoOnlyName(BookEntity book) {
        BookDto bookDto = new BookDto();
        bookDto.setName(book.getName());
        return bookDto;
    }

}
