# Tutorial Keycloak

This project is a tutorial on how to use [Keycloak](https://www.keycloak.org/) in a [Quarkus](https://quarkus.io/) project.

## Table of Contents  
1. [What is an authentication server?](#What_is_an_authentication_server)  
2. [What is Keycloak?](#What_is_Keycloak)  
3. [What can Keycloak do?](#What_can_Keycloak_do)  
4. [What is a protocol for authorization?](#What_is_a_protocol_for_authorization)  
5. [Which are the real differences between OAuth2 and OIDC?](#Which_are_the_real_differences_between_OAuth2_and_OIDC)  
6. [How is the architecture of a project with OIDC?](#How_is_the_architecture_of_a_project_with_OIDC)  
    1. [How the actors interact with each other?](#How_the_actors_interact_with_each_other)  
7. [How is the architecture of a project with SAML2?](#How_is_the_architecture_of_a_project_with_SAML2)  
8. [Setup](#Setup)
    1. [How to locally setup Keycloak for a Quarkus project?](#How_to_locally_setup_Keycloak_for_a_Quarkus_project)  
    2. [How to locally setup Keycloak as an independent server?](#How_to_locally_setup_Keycloak_as_an_independent_server)  
9. [How to configure Keycloak for OIDC?](#How_to_configure_Keycloak_for_OIDC)
    1. [Create roles and users](#Create_roles_and_users)  
10. [Integration with Quarkus](#Integration_with_Quarkus)
    1. [Test with a real project](#Test_with_a_real_project)  
    2. [Configure Swagger](#Configure_Swagger)  
    3. [Add an identity provider](#Add_an_identity_provider) 
11. [Manage user credential via API](#Manage_user_credential_via_API)
    1. [Create user](#Create_user)
    2. [Change password](#Change_password)
    3. [Force a user to change the password](#Force_a_user_to_change_the_password)
    4. [Delete a user](#Delete_a_user)
    5. [Retrive informations of a user](#Retrive_informations_of_a_user)
12. [Grant types](#Grant_types)
    1. [Flows](#Flows)
        1. [Authorization Code Grant](#Authorization_Code_Grant)
        2. [Resource Owner Password Credentials Grant](#Resource_Owner_Password_Credentials_Grant)
        3. [Implicit flow](#Implicit_flow)
        4. [Client Credentials Grant](#Client_Credentials_Grant)
        5. [Proof Key for Code Exchange](#Proof_Key_for_Code_Exchange)
13. [Setup mail](#Setup_mail)
14. [OTP](#OTP)
    1. [Registration](#Registration)
    2. [Login](#Login)
        1. [Login via API with OTP](#Login_via_API_with_OTP)
15. [Other API](#Other_API)
16. [Useful libraries](#Useful_libraries)
    1. [Frontend](#Frontend)
    2. [Backend](#Backend)
17. [References](#References)


<a name="What_is_an_authentication_server"/>

# What is an authentication server?

An authentication server is a specialized server responsible for verifying the identity of users or other services attempting to access a particular resource or service through their credentials, such as usernames and passwords for users.

The authentication server checks the provided credentials and if they are correct it generates a certificate, tipically a token, to allow access. This process helps prevent unauthorized access to sensitive information or systems.

<a name="What_is_Keycloak"/>

# What is Keycloak?

Keycloak is an open-source Identity and Access Management (IAM) solution developed by Red Hat. 

It provides authentication, authorization, and user management for applications and services. 

Keycloak is often used to secure web applications and services, including single sign-on (SSO) capabilities. 

<a name="What_can_Keycloak_do"/>

# What can Keycloak do?

- **Single Sign-On (SSO)**: Keycloak enables users to log in once and gain access to multiple applications without having to enter their credentials repeatedly.

- **User Authentication**: It supports various authentication methods, including username and password, social login (e.g., Google, Facebook), and multi-factor authentication (MFA).

- **User Federation**: Keycloak can integrate with existing user directories or identity providers, such as LDAP, Active Directory, and SAML, allowing organizations to leverage their existing user databases.

- **Authorization**: It offers fine-grained access control and authorization policies to manage who can access what resources within your applications.

- **Identity Brokering**: Keycloak can act as an identity broker, allowing users to authenticate through external identity providers (like OAuth 2.0, OpenID Connect, and SAML) and then use these identities within your application.

- **User Management**: You can manage user accounts, roles, and permissions through the Keycloak admin console or API.

- **Security**: Keycloak provides security features like session management, password policies, and token revocation to enhance the security of your applications.

- **Open Standards**: It adheres to industry standards, making it compatible with a wide range of applications and services.

- **Extensibility**: Keycloak can be extended and customized to meet specific requirements through plugins and themes.

Keycloak is often used in conjunction with web applications, APIs, and microservices to ensure secure and streamlined authentication and authorization.

It simplifies the task of managing user identities and access control, making it a popular choice for organizations looking to implement identity and access management in their software projects **offering variuous types of protocols for authorization**.

<a name="What_is_a_protocol_for_authorization"/>

# What is a protocol for authorization?

An authorization protocol defines the rules and procedures for determining what actions or operations an authenticated user, device, or system is allowed to perform within a given system or resource. Authorization is the process of granting or denying access to specific resources or functionalities based on the permissions associated with an authenticated identity. 

Here are some common authorization protocols:

- **OAuth2**: (or OAuth 2.0) is an industry-standard protocol for **authorization** that enables third-party applications to obtain limited access to a user's resources without exposing their credentials. It is commonly used for delegated authorization, allowing users to grant permissions to external applications or services to access their data on a resource server.
It use access tokens to represent the user's authorization to access specific resources on the resource server. These tokens are presented by the client when making requests to the resource server, this protocol is **JSON-based**.

- **OIDC (OpenID Connect)**: is an authentication layer **built on top of the OAuth 2.0** authorization framework. OpenID Connect provides a standardized way for applications to **authenticate** users, obtain information about them, and enable single sign-on (SSO) across different services and applications.

- **SAML2**: it stands for Security Assertion Markup Language, is an **XML-based** standard for exchanging **authentication and authorization** data between parties, in particular, between an identity provider (IdP) and a service provider (SP). SAML enables single sign-on (SSO) across different web domains or applications.

<a name="Which_are_the_real_differences_between_OAuth2_and_OIDC"/>

# Which are the real differences between OAuth2 and OIDC?

They are related but serve different purposes in the context of identity and access management, such as:

1. **Purpose:**
   - **OAuth 2.0:** It is primarily an **authorization framework**. OAuth 2.0 allows a client application to access a user's resources on a resource server without exposing the user's credentials to the client.
   - **OpenID Connect (OIDC):** It is an **authentication layer built on top of OAuth 2.0** (so it also provide authorization). OIDC focuses on user authentication and provides a way for clients to verify the identity of end-users.

2. **Tokens:**
   - **OAuth 2.0:** The primary token in OAuth 2.0 is the access token, which represents the authorization granted by the user. OAuth 2.0 does not define a standardized way to convey information about the user.
   - **OpenID Connect (OIDC):** Introduces the ID token, which is a JSON Web Token (JWT) containing claims about the authentication event and information about the user.

3. **User Authentication:**
   - **OAuth 2.0:** Does not provide a standardized way to authenticate users. The focus is on obtaining permission (authorization) from the resource owner.
   - **OpenID Connect (OIDC):** Adds an authentication layer to OAuth 2.0, allowing clients to verify the identity of end-users. It specifies how to authenticate users and includes user identity information in the ID token.

4. **Endpoints:**
   - **OAuth 2.0:** Defines endpoints for authorization and token issuance (authorization endpoint and token endpoint).
   - **OpenID Connect (OIDC):** Extends OAuth 2.0 by adding additional endpoints, such as the UserInfo Endpoint, to retrieve information about the authenticated user.

5. **Use Cases:**
   - **OAuth 2.0:** Commonly used for scenarios where a third-party application needs to access resources on behalf of a user without having access to the user's credentials. Examples include API access in mobile apps and delegated authorization.
   - **OpenID Connect (OIDC):** Used for scenarios where user authentication and identity information are essential, such as single sign-on (SSO), identity federation, and social logins.

<a name="How_is_the_architecture_of_a_project_with_OIDC"/>

# How is the architecture of a project with OIDC?

This is its general architecture, in some grant flows the iteraction could be a bit different, expecially when there is no user but it is a Machine-to-Machine (M2M) interaction or a mobile device is involved, but the dynamics are usually the same.

<div align="center">
    <img src="images/Keycloak-Microservice_interaction.PNG" width="50%" height="50%">
</div>

As we can see from the image, the main actors are:

- **Resource Requester**: the client from which the request for a certain resource starts, can be the GUI of an application, Postman, Swagger UI or others.

- **Authentication Server**: the server responsible for verifying that requests arrive from authorized clients and ensuring that they can access certain resources on the server, in our case this element represents Keycloak but there are also others: Okta, SpringBoot Auth, etc...

- **Resource Server**: the application that exposes resources and services to which we want to guarantee security when accessing them, in our case it is a Quarkus microservice but it could also be Spring Boot or others.

<a name="How_the_actors_interact_with_each_other"/>

## How the actors interact with each other?

There are 3 communications blocks in this system:

1. **Resource Requester-Authentication Server**: the client asks Keycloak for a token that can be used to access resources on the server, Keycloak responds with the token if the user is registered on its system or otherwise returns an error.

<div align="center">
    <img src="images/Block1.PNG" width="30%" height="30%">
</div>

2. **Resource Requester-Resource Server**: the client uses the token received from Keycloak to authorize its request on the Quarkus server, if the token is correct the server responds with the requested resource, otherwise it returns an error.

<div align="center">
    <img src="images/Block2.PNG" width="30%" height="30%">
</div>

3. **Authentication Server-Resource Server**: after receiving the request with the token from the client, the Quarkus server sends this token to Keycloak to verify that it is correct, it check that it was not manipulated, it is not expired, the user and the client are registered to access the requested resource. Keycloak can authorize such a transaction from the client or deny it.

<div align="center">
    <img src="images/Block3.PNG" width="30%" height="30%">
</div>

<a name="How_is_the_architecture_of_a_project_with_SAML2"/>

# How is the architecture of a project with SAML2?

<div align="center">
    <img src="images/SAML2_Flow.PNG" width="50%" height="50%">
</div>

The steps of this flow are:

1. **User Access Request (SP-Initiated)**: the user attempts to access a resource or application at the Service Provider (SP) without being authenticated.

2. **SP Sends Authentication Request to IdP**: since the user is not authenticated, the SP generates an authentication request and redirects the user to the Identity Provider (IdP) with this request.

3. **User Authenticates at IdP**: the IdP prompts the user to authenticate by providing credentials, such as a username and password.

4. **IdP Creates SAML Assertion**: upon successful authentication, the IdP generates a SAML assertion. This assertion contains information about the user, such as the user's identity, attributes, and possibly the authentication context.

5. **IdP Sends SAML Assertion to SP**: the IdP sends the SAML assertion back to the SP. This is often done by redirecting the user's browser back to the SP with the SAML assertion included in the request.

6. **SP Validates SAML Assertion**: the SP receives the SAML assertion from the IdP. It validates the assertion's digital signature to ensure its integrity and origin.

7. **SP Grants Access**: upon successful validation of the SAML assertion, the SP creates a session for the user and grants access to the requested resource or application.

8. **User Accesses Resource**: with a valid session established, the user is now authenticated and can access the resource or application at the SP without needing to log in again.

<a name="Setup"/>

# Setup

<a name="How_to_locally_setup_Keycloak_for_a_Quarkus_project"/>

## How to locally setup Keycloak for a Quarkus project?

There are 2 ways:

- **Docker**: 
    we can use a Keycloak image to create a container to which we can send requests, this is very simple to use in Quarkus because if in **.properties** file the `quarkus.oidc.auth-server-url` is not specified, this is the address of the Keycloak server, then Quarkus will assume that we are in **dev mode** and it will automatically pull a Keycloak image and start it in a container (so we must have docker installed on our machine).

    The Keycloak server started can be easily accessed via the Quarkus **DEV UI**.

    This method is obviously only practicable in dev mode, in a **production environment** Keycloak will have to be run as a separate service.

    To work, Keycloak obviously needs a database to have a persistence of the configurations created and the tokens generated, in dev mode by default it uses an h2 database which can receive an initial configuration from a json file that can be indicated via the **.properties** file.

    H2 is a in-memory database, this mean that when we stop the microservice everything that has been changed is lost, the only way to prevent this is to constantly update the json file with the initial configuration before stopping the microservice.

    For this project we will not use this method because:
        - we want to use an architecture more similar to the one that will be used in production.
        - we want to see more clearly how Keycloak organizes and manages its configurations on a real database.
        - using the Keycloak instance downloaded from Quarkus I noticed some errors that sometimes appear, especially when exporting the created realms in a json.

- **Keycloak as an independent server**: 
    this method represents how the interaction between Quarkus and Keycloak will happen in **production**, it requires some initial configuration but it is certainly the best way to learn.

    In the **.properties** file you will need to indicate the URL and port of the Keycloak server that we will start locally.

<a name="How_to_locally_setup_Keycloak_as_an_independent_server"/>

## How to locally setup Keycloak as an independent server?

- **Download Keycloak**: 
    we can download the zip of the project from [here](https://www.keycloak.org/downloads).
- **Create a local database**: 
    we need a database to persist the Keycloak data, Keycloack supports [many databases] (https://www.keycloak.org/server/db) as:
    - **MariaDB Server**
    - **Microsoft SQL Server**
    - **MySQL**
    - **Oracle Database**
    - **PostgreSQL**

    In this example we will create a **PostgreSQL** database named **keycloak** and we don't need to create any table, Keycloak will do it for us.

    ```sql
    CREATE DATABASE keycloak;
    ```
- **Configure Keycloack to use the database**: 
    in the projecy downloaded in the first step there is `conf` folder and inside this a `keycloak.conf` file.

    Open it and inside we can already see many configuration commented, we can uncomment them or write them from scratch, the important thing in the end is to end up with a file with these configurations:

    ```bash
    ## Database

    # The database vendor.
    db=postgres

    # The username of the database user.
    db-username=postgres

    # The password of the database user.
    db-password=postgres

    # The full database JDBC URL. If not provided, a default URL is set based on the selected database vendor.
    db-url=jdbc:postgresql://localhost:5432/keycloak

    ## HTTP

    # Port (Change port because by default keycloak start on port 8080)
    http-port=8081
    ```

    We defined:
    - the database type.
    - username and password to connect to it (probably yours are different).
    - the url, this contains the host, the port and the database name.
    - the port, it is not necessary but by default keycloak start on port 8080 and on that port we want to run the Quarkus microservice.

- **Start Keycloack**: 
    To start Keycloack in a Windows System we have to open a prompt in the top directory of the project and run:

    ```bash
    bin\kc.bat start-dev
    ```

    This command will run the Keycloak server in development mode, to see how to run it on a Linux/Unix system you can open the README.md file in the Keycloack project.

    The first start will take a few seconds because it will setup the database, now it contains many tables.

<a name="How_to_configure_Keycloak_for_OIDC"/>

# How to configure Keycloak for OIDC?
Once started, we can connect to the Keycloak interface [exposed on the port 8081](http://localhost:8081/).

```bash
http://localhost:8081/
```

The first time we need to create an admin user, for simplicity we can create it with **Username** and **Password** as **admin** and next sign in with these credentials.

Logged in as admin we can see the Keycloak administrator console through which we can do many operations, such as:

- **Create a Realm**: 
    *What is a Realm?* A Realm represents an organization, like Amazon or Microsoft. By default exist a **master** realm but we can easily create a new one from the drop-down menu at the top left.

    Let's create a new Realm named `myrealm-dev`, we can also import the configuration of this Realm from a json file but in this case we will just create a blank one.

- **Create a Client**: 
    *What is a Client?* A Client represents an application that an organization provide, like Microsoft Teams or Microsoft Outlook for Microsoft. Each Client is associated to a specific Realm, the Realm selected in the drop-down menu is the one where we are creating it. 
    
    Let's create a new Client with the following parameters:
    - **General Settings**:
        - **Client ID**: `quarkus-be` (we will use this to refer to this client)
        - **Name**: `Quarkus Backend Application`
    - **Capability Config**:
        - **Client authentication**: `turn it on` (we will explain it later)
    - **Login Settings**:
        - **Valid redirect URIs**: `http://localhost:8080/*` (valid paths where redirect the browser after the login, with this path we authorize all the URI that start with **http://localhost:8080/**)
        - **Web origins**: `*` (allowed CORS origins, for the moment we authorize all origins)
    
    Once created the new Client we can see that it has several tabs, the most important are:
    - **Credentials**: this tab contains the **Client secret**, this code is foundamental to obtain the token.
    - **Roles**: roles created for this client, roles can be define at realm level or client level, we can define roles at realm level because some user can access to multiple applications of the same realm.
    - **Client scopes**: 
        client scopes are entities in Keycloak, which are configured at the realm level and they can be linked to clients. The client scopes are referenced by their name when a request is sent to the Keycloak authorization endpoint with a corresponding value of the scope parameter. 
        
        Client scope is a way to limit the roles that get declared inside an access token. When a client requests that a user be authenticated, the access token they receive back will only contain the role mappings you’ve explicitly specified for the client’s scope.

        The default client scope for the client just created is **quarkus-be-dedicated**.
    - **Sessions**: all active sessions for this client, it list all the logged users.

- **Create a User**: 
    a user is a person who can access to the application, let's create a new user, in my case it will have:
    - **Username**: `marco`
    - **Email**: `marco.cordoni@vidiemme.it`
    - **First name**: `Marco`
    - **Last name**: `Cordoni`

    Once created go to the **Credentials** tab of this user and set a password, i will use `marco` for semplicity, tun off the **Temporary** flag so the user mustn't change the password on next login.

<a name="Create_roles_and_users"/>

## Create roles and users
Before starting to test our authentication in a Quarkus project we have to create some roles and users, in the admin console select **Realm roles** and then **Create role** (you can also create the role directly in the client), create 2 roles naming them:

- **admin**
- **manager**

Ignore others tabs, we only need to specify the name.

Now in the **Users** tab click on **Add user** and create a user named **admin**, during the creation you can specify only the **Username** and ignoring other fields.

After clicking on **Create**, select the **Role mapping** tab and and then **Assing role**, choose the roles to assing to this user, we only need of the **admin** role.

Repeat the previous operations for a user named **manager** assigning him the **manager** role.

<a name="Integration_with_Quarkus"/>

# Integration with Quarkus

<a name="Test_with_a_real_project"/>

## Test with a real project
There is a Quarkus application in this repository, you don't need any database, all data are mocked.

To integrate the application with Keycloak we can add these annotation above the endpoints:

- `@PermitAll`: you can use this endpoint without any authentication.

- `@Authenticated`: this endpoint need an authentication but there aren't any role restriction, if no token is provide we will get a **401 Unauthorized** response.

- `@RolesAllowed("admin")`: only tokens of users with the admin role can use it, if you use the token of a non admin user you will get a **403 Forbidden** response.

As an alternative to annotations, we can configure API access policies via the `application.properties` file as follows (but I suggest the first method):

- **To define that all endpoints require authentication**:

```bash
#@Authenticated -> set all endpoints to be authenticated
quarkus.http.auth.permission.authenticated.paths=/*
quarkus.http.auth.permission.public-api.policy=authenticated
```

- **To define that a list of endpoints is accessible without authentication**:

```bash
#@PermitAll -> permit access to all for these endpoints
quarkus.http.auth.permission.public-api.paths=/api/books/findAllWithoutAuthentication, /q/swagger-ui/*, /q/openapi
quarkus.http.auth.permission.public-api.policy=permit
```

- **To define that an endpoint requires a specific role to access it**:

```bash
#@RolesAllowed -> permit access for a role
quarkus.http.auth.policy.manager-role-policy.roles-allowed=manager
quarkus.http.auth.permission.manager-role-api.paths=/api/books/findAllWithAuthenticationAndRoleManager
quarkus.http.auth.permission.manager-role-api.policy=manager-role-policy
```

<a name="Configure_Swagger"/>

## Configure Swagger

To use Swagger we need this dependency:

```xml
<dependency>
    <groupId>io.quarkus</groupId>
    <artifactId>quarkus-smallrye-openapi</artifactId>
</dependency>
```

Now we can access to:

```bash
http://localhost:8080/q/swagger-ui/
```

To allow the access to the **Swagger-UI** we have already added:

```bash
quarkus.http.auth.permission.public-api.paths=/q/swagger-ui/*, /q/openapi
quarkus.http.auth.permission.public-api.policy=permit
```

Finally, we have to add the Authorization Token in Swagger with these properties to let him get the Token from Keycloak:

```bash
quarkus.smallrye-openapi.security-scheme=oidc
quarkus.smallrye-openapi.security-scheme-name=Keycloak
quarkus.smallrye-openapi.oidc-open-id-connect-url=http://localhost:8081/realms/myrealm-dev/.well-known/openid-configuration
```

And add the `@SecurityRequirement(name = "Keycloak")` annotation on the endpoint to define security requirements for the single operation (when applied at method level) or for all operations of a class (when applied at class level):


``` java
...
@GET
@Path("/findAllWithAuthentication")
@SecurityRequirement(name = "Keycloak")
public Response findAllWithAuthentication() {
    logger.info("Find all books");
    List<BookDto> bookDtoList = bookService.findAll();
    return Response.status(Response.Status.OK).entity(bookDtoList).build();
}
...
```

Now, we can obtain the token clicking this button and putting the credentials (same credentials used for the **Get Token** API from Postman):

<div align="center">
    <img src="images/Swagger.PNG" width="30%" height="30%">
</div>

<a name="Add_an_identity_provider"/>

## Add an identity provider

We can add many identity providers to login via Keycloak, let's try to use **GitLab**.

To do it follow these steps:
- in our Keycloak admin page click on `Identity providers` from the left side bar and then click on the GitLab icon (or go to `Add provider -> GitLab` if you already have at least 1 identity provider)
- copy the content of the `Redirect URI` field
- enter in your GitLab account and then go to `User Settings -> Applications`
- click on `Add new application`
- choose a name, for example `Keycloak`
- paste the URL saved in the field `Redirect URI`
- select the option `openid` from the list of options
- save, now you should obtain a page like this, where the **Application ID** and **secret** are two respectively the **Client Id** and **Client Secret** in Keycloak (**WARNING** if you close this page you will not be able anymore to see the secret code, you will only be able to generate a new one):

<div align="center">
    <img src="images/GitLab.PNG" width="70%" height="70%">
</div>

- return to the Keycloak setting page for the GitLab provider and put the **Client Id** and **Client Secret** obtained in the previous step
- save
- now in the login page you should have something like this:

<div align="center">
    <img src="images/GitLab1.PNG" width="30%" height="30%">
</div>

<a name="Manage_user_credential_via_API"/>

# Manage user credential via API

<a name="Create_user"/>

## Create user

To create a new user with a password the steps are:

- get the token of a user with **administrator privileges**, we get a token from the **admin** profile of the realm **master** using the following API of the Keycloak server on the port **8081**, it is a **POST** with the following format:

    ```bash
    http://<host_name>:<port>/realms/<master_realm_name>/protocol/openid-connect/token
    ```

    So it become:

    ```bash
    http://localhost:8081/realms/master/protocol/openid-connect/token
    ```

    It has a body with the following couples key-value:
    - **client_id**: admin-cli (this is the default name for everyone)
    - **grant_type**: password
    - **username**: <admin_username>
    - **password**: <admin_password>

    Save the access token.

- create the user using the following **POST** API:

    ```bash
    http://<host_name>:<port>/admin/realms/<the_realm_where_create_the_user>/users
    ```

    So it become:

    ```bash
    http://localhost:8081/admin/realms/myrealm-dev/users
    ```

    It has the token saved in the previous step in the **authorization bearer token** field and a body with these parameters (it could have many other properties):

    ```json
    {
        "username": "Frankkk",
        "email": "frank@gmail.it",
        "firstName": "Frank",
        "lastName": "Castle",
        "credentials": [
            {
                "type": "password",
                "value": "pass1",
                "temporary": false
            }
        ],
        "requiredActions": [],
        "enabled": true,
        "realmRoles": [
            "offline_access"
        ],
        "attributes": {}
    }
    ```

    If the request is correct it should return a **201 Created** response.

<a name="Change_password"/>

## Change password

To change the password of a user the steps are:

- get the token of a user with **administrator privileges**, this step is identical to the first one of the **Create user** paragraph.

- reset the token using the following **PUT** API:

    ```bash
    http://<host_name>:<port>/admin/realms/<the_realm_of_the_user>/users/<the_id_of_the_user>/reset-password
    ```

    So it become:

    ```bash
    http://localhost:8081/admin/realms/myrealm-dev/users/d4ffa40f-a240-40fd-961d-def46ee248e9/reset-password
    ```

    It has the token saved in the previous step in the **authorization bearer token** field and a body with these parameters:

    ```json
    {
        "type": "password",
        "value": "new_password",
        "temporary": false
    }
    ```

    If the request is correct it should return a **204 No Content** response.

<a name="Force_a_user_to_change_the_password"/>

## Force a user to change the password

To forse a user to change his password the steps are:

- get the token of a user with **administrator privileges**, this step is identical to the first one of the **Create user** paragraph.

- set, on the account on which we want to force the user to update his password, the required action of **Update Password** using the following **PUT** API:

    ```bash
    http://<host_name>:<port>/admin/realms/<the_realm_of_the_user>/users/<the_id_of_the_user>
    ```

    So it become:

    ```bash
    http://localhost:8081/admin/realms/myrealm-dev/users/d4ffa40f-a240-40fd-961d-def46ee248e9/reset-password
    ```

    It has the token saved in the previous step in the **authorization bearer token** field and a body with these parameters:

    ```json
    {
        "requiredActions": [
            "UPDATE_PASSWORD"
        ]
    }
    ```

    If the request is correct it should return a **204 No Content** response.

    To create a user with a temporary password that have to be updated at the first login the body is:

    ```json
    {
        "username": "Frankkk",
        "email": "frank@gmail.it",
        "firstName": "Frank",
        "lastName": "Castle",
        "credentials": [
            {
                "type": "password",
                "value": "pass1",
                "temporary": false
            }
        ],
        "requiredActions": [
            "UPDATE_PASSWORD"
        ],
        "enabled": true,
        "realmRoles": [
            "offline_access"
        ],
        "attributes": {}
    }
    ```   

<a name="Delete_a_user"/>

## Delete a user

To delete a user the steps are:

- get the token of a user with **administrator privileges**, this step is identical to the first one of the **Create user** paragraph.

- delete a user using the following **DELETE** API:

    ```bash
    http://<host_name>:<port>/admin/realms/<the_realm_of_the_user>/users/<the_id_of_the_user>
    ```

    So it become:

    ```bash
    http://localhost:8081/admin/realms/myrealm-dev/users/1ced78f5-8c8f-41a9-9bbb-30916b9741f3
    ```

    It has the token saved in the previous step in the **authorization bearer token** field and no body.
    
    If the request is correct it should return a **204 No Content** response.

<a name="Retrive_informations_of_a_user"/>

## Retrive informations of a user

To get the user info the steps are:

- get the token of a user with **administrator privileges**, this step is identical to the first one of the **Create user** paragraph.

- get the user info using the following **GET** API:

    ```bash
    http://<host_name>:<port>/admin/realms/<the_realm_of_the_user>/users/?username=<the_username>
    ```

    So it become:

    ```bash
    http://localhost:8081/admin/realms/myrealm-dev/users/?username=marco
    ```

    It has the token saved in the previous step in the **authorization bearer token** field and no body.

    If the request is correct it should return a **200 OK** response with a payload like this:

    ```json
    [
        {
            "id": "d4ffa40f-a240-40fd-961d-def46ee248e9",
            "createdTimestamp": 1700824338906,
            "username": "marco",
            "enabled": true,
            "totp": false,
            "emailVerified": false,
            "disableableCredentialTypes": [],
            "requiredActions": [],
            "notBefore": 0,
            "access": {
                "manageGroupMembership": true,
                "view": true,
                "mapRoles": true,
                "impersonate": true,
                "manage": true
            }
        }
    ]
    ```
<a name="Grant_types"/>

# Grant types

Grant types in the context of OAuth 2.0 refer to the different **methods by which a client application can obtain an access** token from an authorization server. The choice of a specific grant involved to modify the preferences on a client within the tab **Settings** in the section **Capability config**. 

<div align="center">
    <img src="images/AccessType1.PNG" width="50%" height="50%">
</div>

Before starting to explain the flows let's analyze other setting:

- **Client authentication**: 
    *"This defines the type of the OIDC client. When it's ON, the OIDC type is set to confidential access type. When it's OFF, it is set to public access type"*

    This is what we can read if we click on the (?) symbol but what does this mean? If we set it to **off**, the tab "Credentials" above will disappear.

    <div align="center">
        <img src="images/AccessType2.PNG" width="50%" height="50%">
    </div>

    This mean that the **Client Secret** is no longer necessary to obtain a token, we just need to pass the **Client ID** and credentials of a user registered on Keycloak. 
    This setting should be keep **on** for server-side applications and **off** for client-side clients that perform browser logins. As it is not possible to ensure that secrets can be kept safe with client-side clients, it is important to restrict access by configuring correct redirect URIs.

- **Authorization**: 
    *"Enable/Disable fine-grained authorization support for a client"*

    If turned **on**, we can see a new tab is appeared above:

    <div align="center">
        <img src="images/AccessType3.PNG" width="50%" height="50%">
    </div>

    In this tab we can create and manage policies and permission to access to the resources.

- **Authentication flow**: 
    list of work flows a user must follow to **login** via their **credential**.

<a name="Flows"/>

## Flows

<a name="Authorization_Code_Grant"/>

### Authorization Code Grant
The Authorization Code Grant type, named **Standard flow** in the Keycloak settings, is used by confidential and public clients to exchange an authorization code for an access token. After the user returns to the client via the redirect URL, the application will get the authorization code from the URL and use it to request an access token.

<div align="center">
    <img src="images/GrantFlow1.PNG" width="50%" height="50%">
</div>

The steps of this flow are the following:

1. User selects Login within application.

2. Auth0's SDK redirects user to Auth0 Authorization Server (`/authorize` endpoint).

3. Auth0 Authorization Server redirects user to login and authorization prompt.

4. User authenticates using one of the configured login options, and may see a consent prompt listing the permissions Auth0 will give to the application.

5. Auth0 Authorization Server redirects user back to application with single-use authorization code.

6. Auth0's SDK sends authorization code, application's client ID, and application's credentials, such as client secret or Private Key JWT, to Auth0 Authorization Server (`/oauth/token` endpoint).

7. Auth0 Authorization Server verifies authorization code, application's client ID, and application's credentials.

8. Auth0 Authorization Server responds with an ID token and access token (and optionally, a refresh token).

9. Application can use the access token to call an API to access information about the user.

10. API responds with requested data.

The related Keycloak endpoints are these 2:

- a **GET** api to access to the relative authentication form:

    ```bash
    http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/auth?
        response_type=code&
        client_id=<client_name>&
        redirect_uri=<url_to_redirect_after_login>&
        scope=openid&
        state=something
    ```

    If inputs are correct we will be redirected to the url passed in the **redirect_uri** param (this url have to be allowed in the client settings), the url of the page where we land will be something like this:

    ```bash
    http://<host_name>:<port>/?
        state=something&
        session_state=f611a46e-720d-4a31-8e30-b36b78b87b11&
        code=931fe9e0-9252-41df-bde5-3eb64f75ffcc.f611a46e-720d-4a31-8e30-b36b78b87b11.9e04c54a-bae3-4a59-9b56-43ae84704518
    ```

    The param in the field **code** will be used in the next step to obtain the token.

- a **POST** api to retrive the access token:

    ```bash
    http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/token
    ```

    With a body in **x-www-form-urlencoded** with these parameters:

    - **client_id**: the client name.
    - **client_secret**: the client secret credential.
    - **grant_type**: `authorization_code`.
    - **code**: the code retrieved in the redirected url of the last API.
    - **redirect_uri**: the uri where we have been redirected in the last API.

#### Should I use this flow?

**No**, the system is vulnerable to a stolen token for the lifetime of that token. For security and scalability reasons, access tokens are generally set to expire quickly so subsequent token requests fail. If a token expires, an application can obtain a new access token using the additional refresh token sent by the login protocol.

<a name="Resource_Owner_Password_Credentials_Grant"/>

### Resource Owner Password Credentials Grant

The Resource Owner Password Credentials Grant type, named **Direct access grants** in the Keycloak settings, is used by REST clients to obtain tokens on behalf of users. Which means that client has access to username/password of user and exchange it directly with Keycloak server for access token.

<div align="center">
    <img src="images/AccessType4.PNG" width="50%" height="50%">
</div>

The steps are:

1. The user clicks Login within the application and enters their credentials.

2. Your application forwards the user's credentials to your Auth0 Authorization Server (`/oauth/token` endpoint).

3. Your Auth0 Authorization Server validates the credentials.

4. Your Auth0 Authorization Server responds with an Access Token (and optionally, a Refresh Token).

5. Your application can use the Access Token to call an API to access information about the user.

6. The API responds with requested data.

The related Keycloak endpoint is a **POST** API:

```bash
http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/token
```

With a body in **x-www-form-urlencoded** with these parameters:

- **client_id**: the client name.
- **client_secret**: the client secret credential.
- **grant_type**: `password`.
- **username**: the username of the user registered in Keycloak.
- **password**: the password of the user registered in Keycloak.

#### Should I use this flow?

**No**, for these reasons:

- ROPC is impersonation, not authentication
- ROPC exposes end-user credentials to applications
- The user cannot consent
- ROPC encourages phishing
- ROPC cannot support Multi-Factor Authentication (MFA)
- ROPC cannot support Single Sign-On (SSO, OpenID Connect only)
- ROPC cannot support federation
- ROPC is insecure for mobile applications and single-page apps
- ROPC has been deprecated by the OAuth working group

<a name="Implicit_flow"/>

### Implicit flow

The Implicit Flow is a browser-based protocol. It is similar to the Authorization Code Flow but it return directly the token in the url insted of the code, this means that there are fewer requests and no refresh tokens.

<div align="center">
    <img src="images/AccessType6.PNG" width="50%" height="50%">
</div>

The protocol works as follows:

1. A user connects to an application using a browser. The application detects the user is not logged into the application.

2. The application redirects the browser to Keycloak for authentication.

3. The application passes a callback URL as a query parameter in the browser redirect. Keycloak uses the query parameter upon successful authentication.

4. Keycloak authenticates the user and creates an identity and access token. Keycloak redirects to the application using the callback URL and additionally adds the identity and access tokens as a query parameter in the callback URL.

5. The application extracts the identity and access tokens from the callback URL.

The related Keycloak endpoint is a **GET** API which redirect to the form login page:

```bash
http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/auth?
    response_type=token&
    client_id=<client_name>&
    redirect_uri=<url_to_redirect_after_login>&
    scope=openid&
    state=something
```

If inputs are correct we will be redirected to the url passed in the **redirect_uri** param (this url have to be allowed in the client settings), the url of the page where we land will be something like this:

```bash
http://localhost:8081/#
    state=something&
    session_state=53a5e798-e383-4b34-b6df-e0e8fbad3fac&
    access_token=eyJh...h9DfxgrmUA&
    token_type=Bearer&
    expires_in=600
```

The param in the field **access_token** is our token.

#### Should I use this flow?

**No**, it is not recommended to use the implicit flow (and some servers prohibit this flow entirely) due to the inherent risks of returning access tokens in an HTTP redirect without any confirmation that it has been received by the client.

<a name="Client_Credentials_Grant"/>

### Client Credentials Grant
The Client Credentials Flow, named **Service accounts roles** in the Keycloak settings, involves an application exchanging its application credentials, such as client ID and client secret, for an access token.

This flow is best suited for **Machine-to-Machine** (**M2M**) applications, such as CLIs, daemons, or backend services, because the system must authenticate and authorize the application instead of a user.

<div align="center">
    <img src="images/GrantFlow2.png" width="50%" height="50%">
</div>

The protocol works as follows:

1. Application sends application's credentials to the Auth0 Authorization Server. To learn more about client authentication methods, read Application Credentials.

2. Auth0 Authorization Server validates application's credentials.

3. Auth0 Authorization Server responds with an access token.

4. Application can use the access token to call an API on behalf of itself.

5. API responds with requested data.

The related Keycloak endpoint to get the token is a **POST** API:

```bash
http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/token
```

With a body in **x-www-form-urlencoded** with these parameters:

- **client_id**: the client name.
- **client_secret**: the client secret credential.
- **grant_type**: `client_credentials`.
- **scope**: `email`.

#### Should I use this flow?

**YES**, **BUT** only from another trusted machine because it do not include informations to authenticate the caller such as username or password.

<a name="Proof_Key_for_Code_Exchange"/>

### Proof Key for Code Exchange
The Proof Key for Code Exchange or **PKCE** isn't a real flow but is build upon the standard **Authorization Code Grant**, to improve security for public clients. It ensures that the application that starts the authentication flow is the same one that finishes it.

<div align="center">
    <img src="images/GrantFlow3.png" width="50%" height="50%">
</div>

The protocol works as follows:

1. The user clicks Login within the application.

2. Auth0's SDK creates a cryptographically-random `code_verifier` and from this generates a `code_challenge`.

3. Auth0's SDK redirects the user to the Auth0 Authorization Server (`/authorize` endpoint) along with the `code_challenge`.

4. Your Auth0 Authorization Server redirects the user to the login and authorization prompt.

5. The user authenticates using one of the configured login options and may see a consent page listing the permissions Auth0 will give to the application.

6. Your Auth0 Authorization Server stores the `code_challenge` and redirects the user back to the application with an authorization code, which is **good for one use**.

7. Auth0's SDK sends this code and the `code_verifier` (created in **step 2**) to the Auth0 Authorization Server (`/oauth/token` endpoint).

8. Your Auth0 Authorization Server verifies the `code_challenge` and `code_verifier`.

9. Your Auth0 Authorization Server responds with an ID token and access token (and optionally, a refresh token).

10. Your application can use the access token to call an API to access information about the user.

11. The API responds with requested data.

Let's implement these step:

1. To enable it in Keycloak:
    - in the tab **Setting** below the section **Capability config** of our client we have to check the field **Standard flow** to enable the **Authorization Code Grant**.
    - in the tab **Advanced** below section **Advanced Settings** of our client there is the field **Proof Key for Code Exchange Code Challenge Method**, this field allow us to choose which code challenge method for PKCE is used. If not specified, keycloak does not applies PKCE to a client unless the client sends an authorization request with appropriate code challenge and code exchange method. This field has 2 options:
        - **plain**: the `code_challenge` is equal to the `code_verifier`.
        - **S256**: 
            this is the most recommended option an the `code_challenge` is calculated from the `code_verifier` using the hash function **SHA256**, the complete function is:
            
            `code_challenge = BASE64URL-ENCODE(SHA256(ASCII(code_verifier)))`

2. Generate the `code_verifier` and the `code_challenge`, for now we can just use [THIS ONLINE TOOL](https://tonyxu-io.github.io/pkce-generator/).

3. a **GET** api to access to the relative authentication form:

    ```bash
    http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/auth?
        response_type=code&
        client_id=<client_name>&
        redirect_uri=<url_to_redirect_after_login>&
        scope=openid&
        state=something&
        code_challenge=<code_challenge_generated_in_step_2>&
        code_challenge_method=S256
    ```

    If inputs are correct we will be redirected to the url passed in the **redirect_uri** param (this url have to be allowed in the client settings), the url of the page where we land will be something like this:

    ```bash
    http://<host_name>:<port>/?
        state=something&
        session_state=1f563414-137a-4816-9e53-6c85a4c43e31&
        code=4cafcb83-8faa-4b61-938e-85475c015c19.1f563414-137a-4816-9e53-6c85a4c43e31.9e04c54a-bae3-4a59-9b56-43ae84704518    
    ```

    The param in the field **code** will be used in the next step to obtain the token.

4. a **POST** api to retrive the access token:

    ```bash
    http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/token
    ```

    With a body in **x-www-form-urlencoded** with these parameters:

    - **client_id**: the client name.
    - **client_secret**: the client secret credential.
    - **grant_type**: `authorization_code`.
    - **code**: the code retrieved in the redirected url of the last API.
    - **redirect_uri**: the uri where we have been redirected in the last API.
    - **code_verifier**: the `code_verifier` generated in the step 2.

#### Is this more safe?

**YES**, without PKCE, An attacker can **intercept** the authorization code that is sent back to the client and exchange it for an access token, which can cause serious data leaks or breaches. One popular method malicious actors use to intercept authorization codes is by registering a malicious application on the user’s device. This malicious application will register and use the same custom URL scheme as the client application, allowing it to intercept redirect URLs on that URL scheme and extract the authorization code.

PKCE addresses this vulnerability by introducing a new flow and three new parameters: `code_verifier`, `code_challenge`, and the `code_challenge_method`. 

Before an authorization request is made, the client creates and stores a secret called `code_verifier`. This is a cryptographically random string that the client uses to identify itself when exchanging an authorization code for an access token. It has a minimum length of 43 characters and a maximum length of 128 characters.

The client also generates a `code_challenge`, which is a transformation of the code verifier. This is sent with the initial authorization request, along with a `code_challenge_method`, the transformation mode used to generate the code challenge.

Next, the `code_challenge` is securely stored by the authorization server, and an authorization code is returned with the redirect URL as usual. When the client wants to exchange this authorization code for the access token, it sends a request that includes the initial `code_verifier`. The server then hashes the code verifier using **SHA-256** (if it has a code challenge method of S256) and encodes the hashed value as a **BASE64URL**. The corresponding value is then compared to the code challenge. If they **match**, an access token is issued. Otherwise, an error message is returned.

This flow ensures that a malicious **third-party application cannot exchange an authorization code for an access token**, since the malicious application does not have the code verifier. Intercepting the code challenge is also useless because **SHA256 is a one-way hashing algorithm and cannot be decrypted**.

#### Should I use this flow?

**YES**, this flow is recommended when a user have to obtain a token, this is not a machine-to-machine protocol.

<a name="Setup_mail"/>

# Setup mail
To setup the smtp provider and anable Keycloak to send e-mail go to `Realm settings` -> `Email` and fill the following fields:

- **From**: the name of the sender, it can be whatever we want, like `keycloak@noreply.it`.
- **Host**: the host of the e-mail provider, in vidiemme we can use our provider `maildev-smtp.k8s.vidiemme.it`.
- **Port**: the port of the host, `1025` for the previous host.
- **Encryption**: select `Enable StartTLS` for smtp.
- **Authentication**: `Enabled`.
- **Username**: the username of the smtp provider.
- **Password**: the password of the smtp provider.

Finally, **test the connection** before continue.

<a name="OTP"/>

# OTP

A **One-Time Password** (OTP) is a password that is valid for only one login session or transaction on a computer system or other digital device. The purpose of using OTPs is to enhance security by providing a dynamic and time-sensitive element to the authentication process.

OTP systems typically work as follows:

1. **Generation:** The OTP is generated by a secure algorithm or mechanism. It can be a numeric code, alphanumeric code, or even a temporary password.

2. **Delivery:** The OTP is then sent to the user through a secure channel. Common delivery methods include text messages, emails, dedicated OTP-generating hardware devices, or mobile apps.

3. **Usage:** The user enters the received OTP during the login process or transaction. The system compares the entered OTP with the one it expects based on the generated code. If the entered OTP is correct and within the valid time window, access is granted or the transaction is approved.

4. **Single Use:** As the name suggests, OTPs are designed for one-time use only. Once used, they become invalid, adding an additional layer of security.

OTP systems are commonly used as part of two-factor authentication (2FA) or multi-factor authentication (MFA) processes. By requiring users to provide something they know (e.g., a password) and something they have (e.g., a temporary OTP), the security of the authentication process is strengthened, making it more challenging for unauthorized individuals to gain access.

<a name="Registration"/>

## Registration
To enable OTP on registration go to `Authentication` -> `Required actions` and set as default action `On` on the `Configure OTP` option, in this way any new user will have this required action assigned to it.

We can manage the OTP option under the tab `Policies` -> `OTP Policy`.

Now, every new user will have the **Required user action** `Configure OTP`, we can also manually assign this action for a user selecting it from the dropdown menu.

To enable registration go to `Realm settings` -> `Login` and enable `User registration` and also `Verify email` to receive a link on the e-mail to confirm the registration (this step require to setup a smtp provider), now we can see the `Register` option in the login page.

<div align="center">
    <img src="images/Otp1.PNG" width="40%" height="40%">
</div>

Clicking on this option we can fill all fields to register us on Keycloak and after pressing the `Register` button we will see this page:

<div align="center">
    <img src="images/Otp2.PNG" width="40%" height="40%">
</div>

With our app, for example `Google Authenticator`, we can scan the Qr Code and get the OTP code to put in the field below.

We have implemented the OTP on registration.

At the end, if we have enable the `Verify email`, an e-mail will be sent to the e-mail address we have provided to confirm our identity via a single use link to complete the registration.

<div align="center">
    <img src="images/Otp4.PNG" width="50%" height="50%">
</div>

Where **FlowTest** is the name of the realm where the user has been created.

<a name="Login"/>

## Login
If we manually assign the **Required user action** `Configure OTP` to a user that already exists and we try to login via his credential now we will see this message:

<div align="center">
    <img src="images/Otp2.PNG" width="40%" height="40%">
</div>

With our app, like `Google Authenticator` we can scan the Qr Code and get the OTP code to put in the field below, at the next login we will just see the OTP field:

<div align="center">
    <img src="images/Otp3.PNG" width="40%" height="40%">
</div>

<a name="Login_via_API_with_OTP"/>

### Login via API with OTP
If we have the OTP enable for a user and we want to access via the **Direct Access Grant** flow we can use the following **POST** API:

```bash
http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/token
```

With a body in **x-www-form-urlencoded** with these parameters:

- **client_id**: the client name.
- **client_secret**: the client secret credential.
- **grant_type**: `password`.
- **username**: the username of the user registered in Keycloak.
- **password**: the password of the user registered in Keycloak.
- **otp**: the OTP code generated on the mobile app.

If the credentials are correct, this API will return the authorization token.

<a name="Other_API"/>

# Other API

- **Refresh Token**: to refresh the token before it expire.

    ```bash
    http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/token
    ```

    The body contains:

    - **client_id**: the client name
    - **client_secret**: the client secret credential
    - **grant_type**: `refresh_token`
    - **refresh_token**: `eyJhbGciOiJIUzI1N....My7wdVNUjtgYU` (the `refresh_token` received from the **Get Token** endpoint)

- **Introspect Token**: to inspect the token and obtain all the relative informations.

    ```bash
    http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/token/introspect
    ```

    The body contains:

    - **client_id**: the client name
    - **client_secret**: the client secret credential
    - **token**: `eyJhbGciOiJIUzI1N....My7wdVNUjtgYU` (the `access_token` received from the **Get Token** endpoint)

- **Logout**: to logout and terminate the session with this token.

    ```bash
    http://<host_name>:<port>/realms/<realm_name>/protocol/openid-connect/logout
    ```

    The body contains:

    - **client_id**: the client name
    - **client_secret**: the client secret credential
    - **refresh_token**: `eyJhbGciOiJIUzI1N....My7wdVNUjtgYU` (the `refresh_token` received from the **Get Token** endpoint)

**WARNING**: these endpoints are useful for directly testing the Keycloak server but in a real project it is not advisable to let Keycloak be queried directly by the front end, because this would require the front end to know sensitive information such as the **client id** and **secret key**.

To avoid this problem we let the server of our project act as an intermediary for the management of tokens.

From the Keycloak admin console we can change the expiration time of the token, the number of time that can be refreshed, the informations it contains and many other things.

<a name="Useful_libraries"/>

# Useful libraries 

<a name="Frontend"/>

## Frontend
On the frontend we have to generate the **challenge code** and the **verifier code** for the Proof Key for Code Exchange flow, in order to to this we can use the Node library [**Crypto**](https://nodejs.org/api/crypto.html).

<a name="Backend"/>

## Backend
On the backend we have to connect our Quarkus microservice to Keycloak using the following dependency:

```xml
<dependency>
    <groupId>io.quarkus</groupId>
    <artifactId>quarkus-keycloak-authorization</artifactId>
</dependency>
```

And putting these configurations on the `application.properties`:

```bash
keycloak.port=<port_number>
keycloak.host=http://localhost:${keycloak.port}
keycloak.realm=<realm_name>

quarkus.oidc.auth-server-url=${keycloak.host}/realms/${keycloak.realm}
quarkus.oidc.client-id=<client_name>
quarkus.oidc.credentials.secret=<client_secret>
quarkus.oidc.tls.verification=none
```

<a name="References"/>

# References

For this guide I used these resources:

- [**Quarkus documentation "Using openid connect (oidc) and Keycloak to centralize authorization"**](https://quarkus.io/guides/security-keycloak-authorization)
- [**Youtube Tutorial for Setup Keycloak on Quarkus**](https://www.youtube.com/watch?v=6xaNsACIq0s&list=PLHXvj3cRjbzsVyj6Pxfu4uRE1PtWa2CIw&ab_channel=DiveIntoDevelopment)
- [**Auth0 Documentation**](https://auth0.com/docs)
- [**Keycloak User Self-Registration**](https://www.baeldung.com/keycloak-user-registration)
- [**Keycloak Two-Factor Authentication**](https://ultimatesecurity.pro/post/2fa/)

# Author

*Marco Cordoni*